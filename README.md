Reproducing results of "An Open Experimental Platform for Ranging, Proximity & Contact Event Tracking using Ultra-Wide-Band & Bluetooth Low-Energy"
==

This technical note is the entry point for reproducing the results of the paper "An Open Experimental Platform for Ranging, Proximity & Contact Event Tracking using Ultra-Wide-Band & Bluetooth Low-Energy".

Experiments are conducted in the [FIT IoT-LAB](https://www.iot-lab.info/) testbed on the Saclay experimentation site based on the [DWM1001-DEV](https://www.decawave.com/product/mdek1001-deployment-kit/) boards. There is currently ten nodes on-site that are identified using the url format `dwm1001-<id=[1-10]>.saclay.iot-lab.info`.
The pictures below show the spatial distribution of the nodes (white boxes) and how they are physically connected to the testbed's gateways (transparent boxes).
<p align="center">
<img src="pics/testbed/testbed-2.jpg" width="200" height="300" style="float: none;padding: 0px">
<img src="pics/testbed/figure1.png" width="400" height="300" style="float: none; padding: 0px">
</p>

>:zap: Note that nodes on the same line are in good Line Of Sight (LOS) conditions, in opposition to the nodes on opposite rows since they are shaded by the wooden pillars 2.5x15 cm thick. For instance, node 5 is in good LOS conditions with nodes 1-4 and in bad LOS conditions with nodes 6-10.


The remainder of this note is organised as follows:

- Section I: specifies the requirements user-side for reproducing the paper results
- Section II: provides an overview of the experimentation workflow and the embedded applications
- Section III: exposes the datasets used for the paper and shows how to replot them (paper's Fig. 6 & Fig. 7)
- Section IV: describes the steps to follow to capture testbed data for rssi-based log-distance models calibration (similar to paper's Fig. 6)
- Section V: describes the steps to follow to capture testbed rssi and twr data for comparing rssi model-based and TWR UWB ranging (similar to paper's Fig. 7)

:warning: Based on this guide, the reader can either:

  - plot the paper results from the datasets by following steps in I.2 and then III;
  - **or** regenerate new datasets as described in sections IV and V assuming he has fulfilled all requirements of step I.

**Contents**

[[_TOC_]]

---

## I) Prerequisites
In order to run the provided scripts, the user must fulfil the following steps:

1. Create an IoT lab account to access the testbed: [signup](https://www.iot-lab.info/testbed/signup) 
2. Have python `3.7` or higher installed along with its `pip` package manager, then do the following to install the required packages, better in a dedicated [virtualenv](https://docs.python.org/3/tutorial/venv.html) to avoid any conflicts:

  ```shell
  $ python --version
    Python 3.7.9

  $ pip install scipy numpy dacite iotlabcli riotctrl matplotlib matplotlib2tikz tabulate tikzplotlib
  ```
  If you are using [conda](https://docs.conda.io/projects/conda/en/latest/index.html), you can alternatively create a new virtual environment using the provided [environment.yml](./environment.yml) file that lists the required python dependencies and creates an environment named `cnert2021`:
  
  ```shell
  $ conda env create -f environment.yml
 
  $ conda env list
    # conda environments:
    #
    base                  *  /Users/rdagher/opt/anaconda3
    cnert2021                /Users/rdagher/opt/anaconda3/envs/cnert2021
  $ conda activate cnert2021
 
 (cnert2021)$ 
  ```
  
  Finally, clone, by recursing submodules, this repository:
  
  ```shell
  $ git clone --recurse-submodules https://gitlab.inria.fr/pepper/cnert-2021.git
  ```
  
3. Authentificate on the testbed (this will store a hidden credentials file in your homedir eg. `$HOME/.iotlabrc`):

  ```shell
  $ iotlab-auth -u <login>
  ```
  where `<login>` is the username for connecting to the testbed (from step 1) and the password will be prompted.
  
4. Test your installation and remote access to the testbed by showing the available dwm1001 nodes (url, position, etc.)

  ```shell
  $ iotlab-status --nodes --site saclay --archi dwm1001
  ```
the output is a json list of nodes information. If this fails, check your credentials in step 3

  ```json
  {
      "items": [
          {
              "archi": "dwm1001:dw1000",
              "camera": 0,
              "mobile": 0,
              "mobility_type": " ",
              "network_address": "dwm1001-1.saclay.iot-lab.info",
              "site": "saclay",
              "state": "Alive",
              "uid": " ",
              "x": "5.52",
              "y": "68.3",
              "z": "6"
          },
          ...
          ...
          {
              "archi": "dwm1001:dw1000",
              "camera": 0,
              "mobile": 0,
              "mobility_type": " ",
              "network_address": "dwm1001-9.saclay.iot-lab.info",
              "site": "saclay",
              "state": "Alive",
              "uid": " ",
              "x": "4.34",
              "y": "64.6",
              "z": "6"
          }
      ]       
  ```
5. Have a complete RIOT build environment, since the runner script builds the embedded applications firmwares prior deploying the experiment.
   This step is required if the user wants to collect new datasets using the experiments runner script.
  
   **Option 1: Docker-based build**
   Have docker installed with the `riot/riotbuild` image pulled
   
   ```shell
   $ docker pull riot/riotbuild
   ```
   and add this line to your `.bashrc` or execute it in your terminal before running the experiments scripts (runner script)
   
   ```shell
   $ export BUILD_IN_DOCKER=1
   ```
   
   **Option 2: native build (Linux/Mac)**
   - GNU Arm Embedded Toolchain: we used this version `GNU Arm Embedded Toolchain 9-2020-q2-update 9.3.1 20200408 (release)`
   - GNU Make version 4.0: we used `GNU Make 4.3`

## II) Experimentation details
The experiments are conducted remotely from the user's machine thanks to the python package [iotlabcli](https://pypi.org/project/iotlabcli/) throughout the FIT IoT-LAB Restful [API](https://www.iot-lab.info/docs/getting-started/design/) on top of http. This allows not only the same user functionalities as on the web portal, but also the full automation of the experiments thanks to scripting tools: scheduling, flashing the firmware, collecting data from serial interface and more.

### II.A) Embedded applications
- [riot-demo-apps/01\_uwb\_rng](https://gitlab.inria.fr/pepper/riot-demo-apps/-/tree/master/01_uwb_rng): This application implements a full UWB TWR ranging command-line application on top of the DWM1001 device. The application can be configured as tag or anchor and thus perform ranging measurements. The RSSI metrics of the UWB anchor-tag link is also reported.
- [riot-demo-apps/02\_ble\_scan\_rss](https://gitlab.inria.fr/pepper/riot-demo-apps/-/tree/master/02_ble_scan_rss): This application implements a full BLE Advertisement/Scanning mechanism on top of the DWM1001 device. Thanks to the command-line interface,  the application can be configured to advertise only or to sniff the advertisement packets by reporting the advertiser id and the RSSI of the BLE link. 

:zap: Note that these applications can be compiled individually and tested on the FIT IoT-Lab portal. More details are available in the links above.
 
### II.B) General workflow
The workflow is simplified thanks to a runner script that will do the following steps:

1. build the two embedded applications: [riot-demo-apps/01\_uwb\_rng](https://gitlab.inria.fr/pepper/riot-demo-apps/-/tree/master/01_uwb_rng) and [riot-demo-apps/02\_ble\_scan\_rss](https://gitlab.inria.fr/pepper/riot-demo-apps/-/tree/master/02_ble_scan_rss)
2. reserve the list of dwm1001 nodes by submitting an experience on the testbed's Saclay site
3. Perform UWB TWR ranging between initiator node and its neighbors
    - Flash all the nodes with the TWR UWB ranging application
    - Configure the initiator node as a tag
    - Configure the rest of the nodes as anchor
    - For each initiator-neighbor (i.e tag-anchor) pair, perform 100 ranging sequences while decoding initiator's serial output into an experiment object cache
    - Stop the nodes
4. Perform BLE RSSI tracing between initiator node and its neighbors
    - Flash all the nodes with the BLE rss scan application
    - Configure the all the nodes but the initiator to send BLE advertisements
    - Configure initiator node to scan for 100 rounds while decoding its serial output into the experiment object cache
    - Stop the nodes
5. Serialize the experiment object cache into a human-readbale json file containing the full metadata and metrics fo interest (twr+rssi for UWB and rssi for BLE)

>:zap: This workflow is run in sections IV to collect rssi data for calibrating path-loss log-distance models, then again in section V to collect confirmation data in order to evaluate the calibrated models and compare them against the TWR range measurements.

## III) Exposed datasets
This section provides an overview of the [datasets](./datasets) discussed in the paper's Fig.6 and Fig.7 respectively. 

| Dataset | Description |
|---------|-------------|
| [01\_rss\_calibration\_dataset.json](./datasets/01_rss_calibration_dataset.json) | Dataset used to calibrate the RSSI log-distance model for BLE and UWB data |
| [01\_rss\_pl\_model.json](./datasets/01_rss_pl_model.json) | Representation of the RSSI log-distance model for BLE and UWB data, when loaded allows mapping any BLE/UWB RSSI measurement to a distance in meters |
| [02\_ranging\_comparison_dataset.json](./datasets/02_ranging_comparison_dataset.json) | Confirmation dataset used to check the prediction errors of a calibrated RSSI log-distance model against TWR provided by the UWB and ground truth distances exposed by the testbed |

For more details on the internal data representation refer to [datasets/Readme.md](./datasets).

As discussed in section II.B, the workflow behind the datasets is the following:

```mermaid
graph TB
    A(Runner Script) -->| 01_rss_calibration_dataset.json | B(Calibration Script) -->| 01_rss_pl_model.json | D(Comparison Script)
    C(Runner Script) -->| 02_ranging_comparison_dataset.json | D(Comparison Script)
    %% Node styling
    style A fill:#f7a,stroke:#333,stroke-width:4px
    style C fill:#f7a,stroke:#333,stroke-width:4px
```

### III.A) RSSI log-distance model calibration dataset
In order to plot the data of this [dataset](datasets/01_rss_calibration_dataset.json), run the following

```shell
$ python riot-demo-apps/dist/tools/utils/plot_rss_calibration.py datasets/01_rss_calibration_dataset.json
```
This will do the RSSI calibration and plot the following [figures](pics/datasets/01_rss_calibration/):

| Figure   | Description                                                               |
|----------|---------------------------------------------------------------------------|
| figure 1 | log-distance path-loss model fit for the **BLE** RSSI data                |
| figure 2 | Modeling errors boxplot for the log-distance path-loss model (**BLE**)    |
| figure 3 | Modeling errors statistics for the log-distance path-loss model (**BLE**) |
| figure 4 | log-distance path-loss model fit for the **UWB** RSSI data                |
| figure 5 | Modeling errors boxplot for the log-distance path-loss model (**UWB**)    |
| figure 6 | Modeling errors statistics for the log-distance path-loss model (**UWB**) |

### III.B) Comparing RSSI-based model ranging and UWB time-based ranging (TWR)
In order to plot the data of this [dataset](datasets/02_ranging_comparison_dataset.json), run the following

```shell
$ python riot-demo-apps/dist/tools/utils/plot_exp_data.py datasets/02_ranging_comparison_dataset.json datasets/01_rss_pl_model.json 
```
where the first parameter is a dataset different from the one that generated the log-distance model passed as second parameter.

This wil plot the following [figures](pics/datasets/02_ranging_comparison/):

| **Figure** | **Description** |
|------------|-----------------|
| figure 1 | Spatial distributions of the dwm1001 nodes in the 3D space |
| figure 2 | Inter-nodes euclidean distances sorted by increasing distance w.r.t the initiator node |
|  |  |
| figure 3 | Histogram of the **BLE** raw RSSI data as seen from the initiator node (id=5) |
| figure 4 | Boxplot of the **BLE** raw RSSI data as seen from the initiator node (id=5) |
| figure 5 | Satistics of the **BLE**raw RSSI data as seen from the initiator node (id=5) |
| figure 6 | Boxplot of the log-distance model prediction errors for the **BLE** RSSI data |
| figure 7 | Statistics og the log-distance model prediction errors  for the **BLE** RSSI data |
|  |  |
| figure 8 | Histogram of the **UWB** raw RSSI data as seen from the initiator node (id=5) |
| figure 9 | Boxplot of the **UWB** raw RSSI data as seen from the initiator node (id=5) |
| figure 10 | Satistics of the **UWB** raw RSSI data as seen from the initiator node (id=5) |
| figure 11 | Boxplot of the log-distance model prediction errors for the **UWB** RSSI data |
| figure 12 | Statistics of the log-distance model prediction errors  for the **UWB** RSSI data |
|  |  |
| figure 13 | Boxplot of the **UWB** raw TWR range data as seen from the initiator node (id=5) |
| figure 14 | Satistics of the **UWB** raw TWR range data as seen from the initiator node (id=5) |
| figure 15 | Boxplot of the **UWB** TWR range errors as seen from the initiator node (id=5) |
| figure 16 | Statistics of the **UWB** TWR range errors as seen from the initiator node (id=5) |

:warning: Note that only figures 15-16 (UWB TWR), 11-12 (UWB RSSI) and 6-7 (BLE RSSI) are used in the paper, the rest are kept for the record.

## IV) Reproduce: RSSI model Calibration results

In this section we show how to generate a dataset for calibrating a log-distance model based on the RSSI for both BLE and UWB channels.
This can be achieved in two steps:

1. Run the experiment runner script. Noting that at the end of the experiment, the output is saved into the folder `./log_ref_10`
    
    ```shell
    $ export PYTHONPATH=riot-demo-apps/RIOT/dist/pythonlibs
    $ python riot-demo-apps/dist/tools/runner.py ./riot-demo-apps/ --d log_ref_10
    ```

This will generate a file `./log_ref_10/json_dump.txt` containing the data.

2. Run the calibration script and check the plotted output
    
    ```shell
    $ export PYTHONPATH=riot-demo-apps/RIOT/dist/pythonlibs
    $ python riot-demo-apps/dist/tools/utils/plot_rss_calibration.py log_ref_10/json_dump.txt
    ```

This will produce the following output:
 - Figures: a total of 6 figures detailed in section III.A. They are also save in the folder  `./model-plots/figure*.png`, along with other formats for use in latex.
 - BLE+UWB Log-distance model json representation (can be used in section V): available in the json file `./model-plots/rss_pl_model.json`

> :warning: Referring to the datasets described in section III, this executes the first branch ` Runner Script –—> Calibration Script `

## V) Reproduce: Comparing RSSI-based model ranging and UWB time-based ranging (TWR)
In this section we show how to generate a dataset for comparing UWB TWR ranging and BLE/UWB log-distance rssi-based model predictions against ground-truth internode distances provided by the testbed.
This can be achieved in two steps:

1. Run the experiment runner script. Noting that at the end of the experiment, the output is saved into the folder `./log_conf_10`
    
    ```shell
    $ export PYTHONPATH=riot-demo-apps/RIOT/dist/pythonlibs
    $ python riot-demo-apps/dist/tools/runner.py ./riot-demo-apps/ --d log_conf_10
    ```

This will generate a file `./log_conf_10/json_dump.txt` containing the data.

2. Run the comparison script and check the output. The second parameter is the path-loss model taken here from the public dataset, but can be changed to the one from section IV.

    ```shell
    $ export PYTHONPATH=riot-demo-apps/RIOT/dist/pythonlibs
    $ python riot-demo-apps/dist/tools/utils/plot_exp_data.py log_conf_10/json_dump.txt datasets/01_rss_pl_model.json
    ```

This will produce a total of 16 figures detailed in section III.B.

> :warning: Referring to the datasets described in section III, this executes the second branch `Runner Script –—> Comparison Script`

 