Datasets details
==

This folder contains the following datasets following an object serialization scheme:

| Dataset | Description |
|---------|-------------|
| [01\_rss\_calibration\_dataset.json](./01_rss_calibration_dataset.json) | Dataset used to calibrate the RSSI log-distance model for BLE and UWB data |
| [01\_rss\_pl\_model.json](./01_rss_pl_model.json) | Representation of the RSSI log-distance model for BLE and UWB data, when loaded allows mapping any BLE/UWB RSSI measurement to a distance in meters |
| [02\_ranging\_comparison_dataset.json](./02_ranging_comparison_dataset.json) | Confirmation dataset used to check the prediction errors of a calibrated RSSI log-distance model against TWR provided by the UWB and ground truth distances exposed by the testbed |


Dataset fields description is given below. 

:zap: In order to "navigate" through a dataset json files it is recommended to paste its content online on [jsonviewer](http://jsonviewer.stack.hu/) in the text pane, and then click on the viewer pane to go through the fields.

#### 01\_rss\_calibration\_dataset.json <br> \& 02\_ranging\_comparison\_dataset.json
![Experiment dataset](./pics-description/runner-output-dataset-description.png)

#### 01\_rss\_pl_model.json
![Experiment dataset](./pics-description/rss_pl_model.json-description.png)

